package org.czi.kb.fileingestion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.czi.rato.api.blockstore.BlockStoreFactory;
import org.czi.rato.api.cluster.service.MetaServiceContext;
import org.czi.rato.api.cluster.service.Ticket;
import org.czi.rato.api.cluster.service.coordination.Job;
import org.czi.rato.api.cluster.service.coordination.Task;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.data.SessionFactory;
import org.czi.rato.api.data.Transaction;
import org.czi.rato.api.data.queue.NamedQueue;
import org.czi.rato.api.data.queue.QueueMessage;
import org.czi.rato.api.data.queue.QueueProvider;
import org.czi.rato.api.model.datagrid.Publication;
import org.czi.rato.api.model.datagrid.Researcher;
import org.czi.rato.testing.service.Before;
import org.czi.rato.testing.service.After;
import org.czi.rato.testing.service.ServiceTest;
import org.czi.rato.testing.service.Test;
import org.czi.rato.testing.service.TestEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by jlee on 11/05/17.
 */
@ServiceTest(serviceClass = FileIngestion.class, environment = TestEnvironment.SILVER)
public class FileIngestionServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(FileIngestionServiceTest.class);
    private static final String JOB_QUEUE = "jobQueue";
    private static final String SERVICE = "org.czi.kb.fileingestion.FileIngestion";

    @Autowired
    private MetaServiceContext metaServiceContext;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private DataRepository<UUID, Task> taskDataRepository;

    @Autowired
    private DataRepository<UUID, Job> jobDataRepository;

    @Autowired
    private QueueProvider queueProvider;

    @Autowired
    private BlockStoreFactory blockStoreFactory;


    @Autowired
    private DataRepository<Long, Publication> publicationDataRepository;


    /**
     * Send tickets to start the job.
     */
    @Before
    public void setup() throws InterruptedException {
        logger.info("Setting up FileIngestionServiceTest");

        final Map<String, String> metadata = new HashMap<>();

        // Hardcode file location
        metadata.put(FileIngestion.FILEPATH, "/home/jlee/repos/file-ingestion/service/src/test/resources/fuse-test-data.tar.gz");

        createIngestionJob(metadata);
        Thread.sleep(30000);
    }

    private void createIngestionJob(final Map<String, String> metadata) {
        final Transaction transaction = sessionFactory.getOrCreateSession().startTransaction();
        final Job job = jobDataRepository.newInstance();
        final UUID jobId = UUID.randomUUID();
        job.setId(jobId);

        final Task task = taskDataRepository.newInstance();
        final UUID taskId = UUID.randomUUID();
        task.setId(taskId);
        task.setJob(job);
        task.setMetadata(metadata);
        task.setService(SERVICE);
        taskDataRepository.save(task);

        job.getTasks().add(task);
        jobDataRepository.save(job);

        sessionFactory.getOrCreateSession().save(job);
        transaction.commit();

        final QueueMessage<UUID> jobMessage = new QueueMessage<>(jobId);
        try {
            final NamedQueue<UUID> queue = queueProvider.getNamedQueue(JOB_QUEUE);
            queue.put(jobMessage);
        } catch (final InterruptedException exception) {
            logger.error("Error in putting job message into named queue", exception);
            Thread.currentThread().interrupt();
        }
    }


    @After
    public void teardown(){
        logger.info("After running");
    }


    @Test
    public void testFileIngestion() {
        logger.info("Testing File Ingestion");

        // First line in document.csv
        /*
         * 19125112,19125112,"{}",19125112,"pubmed",
         * "Abstracts of the 9th Annual International Meeting on Simulation in Healthcare.
         *          January 11-14, 2009. Lake Buena Vista, Florida, USA",
         *          " ","2010-01-01","{null}","English","{null}","{null}"
         */
        final Publication first = publicationDataRepository.query().equals("ExternalId", "19125112").getOne();

        assertEquals(first.getPublisher(), "pubmed");
        assertEquals(first.getArticleTitle(), "Abstracts of the 9th Annual International Meeting " +
                "on Simulation in Healthcare. January 11-14, 2009. Lake Buena Vista, Florida, USA");
        assertEquals(first.getAbstractText(), " ");



        // Last line in document.csv
        /*
         * 1000004331,1000004331,"{}",1000004331,"","10.1007/s12678-014-0206-1","{null}",
         * 2016,"{null}","English","{null}","{null}"
         */
        final Publication last = publicationDataRepository.query().equals("ExternalId", "1000004331").getOne();

        assertEquals(last.getPublisher(), "");
        assertEquals(last.getArticleTitle(), "10.1007/s12678-014-0206-1");
        assertEquals(last.getAbstractText(), "{null}");
    }
}
