#!/usr/bin/env bash
set -u -x -e

cd $PWD/..

MACHINE_IP=`docker ps | grep meta-data | head -n1 | docker inspect \`awk '{print $1}'\` | grep IPAddress | tail -n1 | awk '{print $2}' | cut -d "\"" -f 2`

echo "uploading service $SERVICE_NAME to $MACHINE_IP"
curl -H "Content-Type: application/json" --data '' $MACHINE_IP:8335/v1/services/startCoordination

