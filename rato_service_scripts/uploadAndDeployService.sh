#!/usr/bin/env bash
set -u -x -e

cd $PWD/..
SRCDIR=$PWD
SERVICE_NAME=`basename $SRCDIR`
MACHINE_IP=`docker ps | grep meta-data | head -n1 | docker inspect \`awk '{print $1}'\` | grep IPAddress | tail -n1 | awk '{print $2}' | cut -d "\"" -f 2`
SERVICE_VERSION="1.0-SNAPSHOT.jar"
SERVICE_JAR_FILE=$SRCDIR/target/$SERVICE_NAME-$SERVICE_VERSION

#service specific info
majorVersion=1
minorVersion=1
maxPerNode=1
instances=1
type="Distributed"


#

if [ -f $SERVICE_JAR_FILE ];
then
    echo "found $SERVICE_JAR_FILE"
    echo "uploading service $SERVICE_NAME to $MACHINE_IP"
    curl -H "Content-Type: multipart/form-data" -F \"upload=$SERVICE_JAR_FILE\" $MACHINE_IP:8335/v1/services/uploadLatestVersion
    echo "Deploying service $SERVICE_NAME to $MACHINE_IP"
    curl -H "Content-Type: application/json" --data "{\"serviceName\":\"org.czi.rato.services.$SERVICE_NAME\",\"majorVersion\":$majorVersion,\"minorVersion\":$minorVersion,\"maxPerNode\":$maxPerNode,\"instances\":$maxPerNode,\"type\":\"$type\"}" $MACHINE_IP:8335/v1/services/startService
else
    echo "$SERVICE_JAR_FILE was not found"
fi