#!/usr/bin/env bash

set -u -x -e

cd "$(dirname "$0")"
SRCDIR=$PWD/../..

if [ ! -f docker-compose.yml ]; then
    if [ ! -f $SRCDIR/tasks/docker-compose-groups/ci_rato_services.yml ]; then
        echo "Please run tasks/init_hecate.sh first"
        exit
    fi
    cp $SRCDIR/tasks/docker-compose-groups/ci_rato_services.yml docker-compose.yml
    cp $SRCDIR/tasks/docker-compose/common-2.yml common.yml
fi

docker-compose build
docker-compose up -d --remove-orphans
docker-compose logs -f