#!/bin/bash

# SOURCE: https://github.com/sciencescape-net/Vulcan-Forge/blob/master/examples-rato-service/DIRNAME/FILENAME

set -u -e -x
set -o pipefail

ERR=${ERR:-/dev/null}; IMG='sciencescapenet/forge:release'; docker pull $IMG && \
   docker run --rm --entrypoint /route $IMG \
   examples-rato-service/$(basename $(dirname "$BASH_SOURCE"))/$(basename "$BASH_SOURCE") 2>>$ERR | ORIG_SOURCE=$BASH_SOURCE bash
