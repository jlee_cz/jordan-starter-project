#!/bin/bash

set -u -e -x

echo "APP_HOME: $APP_HOME"
cd $APP_HOME
REPO=main

CONTEXT_PATH=/opt/context
echo "CONTEXT_PATH: $CONTEXT_PATH"

cd $APP_HOME
git archive --format=tar --prefix=$REPO/ HEAD | (cd /tmp/ && tar xf -)
echo "INFO: $REPO repository extracted"
ls -la /tmp/$REPO

BUILD_INFO_FILE=$CONTEXT_PATH/build_info.txt
echo "INFO: Build info file: $BUILD_INFO_FILE"
git log -n 1 > $BUILD_INFO_FILE
cat $BUILD_INFO_FILE

cd /tmp/$REPO
echo "INFO: Compile jars.."
mvn install -Dmaven.test.skip=true

cp -vR ./service/target/*.jar $CONTEXT_PATH/
cp -vR ./testing/target/*.jar $CONTEXT_PATH/
