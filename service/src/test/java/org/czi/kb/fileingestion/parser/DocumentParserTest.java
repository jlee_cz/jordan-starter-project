package org.czi.kb.fileingestion.parser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.Document;
import org.junit.Test;

/**
 * Created by jlee on 08/05/17.
 */
public class DocumentParserTest {
    @Test(expected = ParseException.class)
    public void parseEmpty() throws Exception {
        final DocumentParser parser = new DocumentParser();
        parser.parse("");
    }


    @Test
    public void parseValid() throws ParseException, IOException {
        final DocumentParser documentParser = new DocumentParser();
        Document document = documentParser.parse(
                "19553755,19553755,\"{}\",19553755,\"pubmed\","
                        + "\"Atherosclerosis and the role of inflammation\",\"shortened for simplicity\","
                        + "\"2010-04-07\",\"{null}\",\"English\",\"{null}\",\"{null}\"");


        assertEquals(document.getDocumentId(), "19553755");
        assertEquals(document.getPublisher(), "pubmed");
        assertEquals(document.getPaperTitle(), "Atherosclerosis and the role of inflammation");
        assertEquals(document.getAbstractInfo(), "shortened for simplicity");
        assertEquals(document.getLanguage(), "English");
        assertEquals(document.getPublicationDate(), "2010-04-07");


    }

    @Test
    public void case1() throws ParseException, IOException {
        final DocumentParser documentParser = new DocumentParser();
        Document document = documentParser.parse("19125112,19125112,\"{}\",19125112,\"pubmed\","
                + "\"..title..\",\"..abstract..\",\"2010-01-01\",\"{null}\",\"English\",\"{null}\",\"{null}\"");

        assertEquals(document.getDocumentId(), "19125112");
        assertEquals(document.getPublisher(), "pubmed");
        assertEquals(document.getPaperTitle(), "..title..");
        assertEquals(document.getAbstractInfo(), "..abstract..");
        assertEquals(document.getLanguage(), "English");
        assertEquals(document.getPublicationDate(), "2010-01-01");
    }

    @Test
    public void case2() throws ParseException, IOException {
        final DocumentParser documentParser = new DocumentParser();
        Document document = documentParser.parse("15466631,15466631,\"{}\",15466631,\"\",15466631,\"{null}\","
                + "1900,\"{null}\",\"English\",\"{null}\",\"{null}\"");

        assertEquals(document.getDocumentId(), "15466631");
        assertEquals(document.getPublisher(), "");
        assertEquals(document.getPaperTitle(), "15466631");
        assertEquals(document.getAbstractInfo(), "{null}");
        assertEquals(document.getLanguage(), "English");
        assertEquals(document.getPublicationDate(), "1900");
    }

    @Test
    public void case3() throws ParseException, IOException {
        final DocumentParser documentParser = new DocumentParser();
        Document document = documentParser.parse(
                "1000005235,1000005235,\"{}\",1000005235,\"\",\"10.1111/tct.12416\",\"{null}\",2015,"
                        + "\"{null}\",\"English\",\"{null}\",\"{null}\"");

        assertEquals(document.getDocumentId(), "1000005235");
        assertEquals(document.getPublisher(), "");
        assertEquals(document.getPaperTitle(), "10.1111/tct.12416");
        assertEquals(document.getAbstractInfo(), "{null}");
        assertEquals(document.getLanguage(), "English");
        assertEquals(document.getPublicationDate(), "2015");
    }

    @Test (expected = ParseException.class)
    public void parseInvalid() throws ParseException, IOException {
        final DocumentParser documentParser = new DocumentParser();
        Document document = documentParser.parse("blah,invalid\"\"");

    }
}
