package org.czi.kb.fileingestion.transformer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.datagrid.Citation;
import org.czi.rato.api.model.datagrid.Publication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Using @Mock annotation and instantiating test object inside @Test.
 */
@RunWith(MockitoJUnitRunner.class)
public class CitationTransformerTest {

    @Mock (answer = Answers.RETURNS_DEEP_STUBS)
    private static DataRepository<Long, Publication> pubDataRepository;

    @Mock
    private static DataRepository<Long, Citation> citDataRepository;

    @Mock
    private static Citation citationRato;

    @Mock
    private static org.czi.kb.fileingestion.objects.Citation citationJava;

    @Test
    public void transformCitation() {
        final CitationTransformer citationTransformer = new CitationTransformer(pubDataRepository,citDataRepository);

        when(citationJava.getPaperId()).thenReturn("paperId");
        when(citationJava.getPaperReferenceId()).thenReturn("refId");

        when(citDataRepository.newInstance()).thenReturn(citationRato);

        when(pubDataRepository.query().equals(any(), eq("paperId")).getOne()).thenReturn(mock(Publication.class));
        when(pubDataRepository.query().equals(any(), eq("refId")).getOne()).thenReturn(mock(Publication.class));

        citationTransformer.transform(citationJava);

        verify(citDataRepository).newInstance();
    }
}