package org.czi.kb.fileingestion.transformer;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.datagrid.Institution;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Using @Mock annotation and instantiating test object globally (need @InjectMocks).
 */
@RunWith(MockitoJUnitRunner.class)
public class InstitutionTransformerTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private static DataRepository dataRepository;

    @Mock
    private static Institution institutionRato;

    @Mock
    private static org.czi.kb.fileingestion.objects.Institution institutionJava;

    @InjectMocks
    private static InstitutionTransformer institutionTransformer;

    @Test
    public void transformInstitution() {
        when(institutionJava.getInstitutionId()).thenReturn("id");
        when(institutionJava.getInstitutionName()).thenReturn("name");
        when(institutionJava.getInstitutionCity()).thenReturn("city");

        when(dataRepository.newInstance()).thenReturn(institutionRato);

        institutionTransformer.transform(institutionJava);

        verify(dataRepository).newInstance();
    }
}