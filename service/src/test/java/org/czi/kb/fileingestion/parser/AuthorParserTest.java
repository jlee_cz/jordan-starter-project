package org.czi.kb.fileingestion.parser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.Author;
import org.junit.Test;

/**
 * Created by jlee on 08/05/17.
 */
public class AuthorParserTest {

    @Test(expected = ParseException.class)
    public void parseEmpty() throws Exception {
        final AuthorParser parser = new AuthorParser();
        parser.parse("");
    }



    @Test
    public void parseValid() throws ParseException, IOException {
        final AuthorParser authorParser = new AuthorParser();
        Author author = authorParser.parse("127367624,127367624,\"{}\",\"Matías\",\"Arteaga\",\"{null}\"");

        assertEquals(author.getAuthorName(), "Matías Arteaga");
        assertEquals(author.getAuthorId(), "127367624");

    }

    @Test (expected = ParseException.class)
    public void parseInvalid() throws ParseException, IOException {
        final AuthorParser authorParser = new AuthorParser();
        Author author = authorParser.parse("127367624,127367624,\"{}\",\"Arteaga\",\"{null}\"");

    }
}
