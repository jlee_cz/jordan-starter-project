package org.czi.kb.fileingestion.parser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.Authorship;
import org.junit.Test;

/**
 * Created by jlee on 08/05/17.
 */
public class AuthorshipParserTest {

    @Test(expected = ParseException.class)
    public void parseEmpty() throws Exception {
        final AuthorshipParser parser = new AuthorshipParser();
        parser.parse("");
    }



    @Test
    public void parseValid() throws ParseException, IOException {
        final AuthorshipParser AuthorshipParser = new AuthorshipParser();
        Authorship authorship = AuthorshipParser.parse("127331055,26717796,\"{null}\"");

        assertEquals(authorship.getAuthorId(), "127331055");
        assertEquals(authorship.getPaperId(), "26717796");
        assertEquals(authorship.getInstitutionId(), "{null}");
    }

    @Test
    public void parseValid2() throws ParseException, IOException {

        final AuthorshipParser AuthorshipParser = new AuthorshipParser();
        Authorship authorship = AuthorshipParser.parse("127367624,25353639,\"14395\"");

        assertEquals(authorship.getAuthorId(), "127367624");
        assertEquals(authorship.getPaperId(), "25353639");
        assertEquals(authorship.getInstitutionId(), "14395");
    }

    @Test (expected = ParseException.class)
    public void parseInvalid() throws ParseException, IOException {
        final AuthorshipParser AuthorshipParser = new AuthorshipParser();
        Authorship authorship = AuthorshipParser.parse("This is invalid, Invalid stuff");

    }
}
