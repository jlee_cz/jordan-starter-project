package org.czi.kb.fileingestion.transformer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.czi.kb.fileingestion.objects.Document;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.data.Identifiable;
import org.czi.rato.api.model.datagrid.Publication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Using @Mock annotation and instantiating test object inside @Test.
 */
@RunWith(MockitoJUnitRunner.class)
public class DocumentTransformerTest {

    @Mock (answer = Answers.RETURNS_DEEP_STUBS)
    private static DataRepository<Long, Publication> dataRepository;

    @Mock
    private static Document document;

    @Mock
    private static Publication publication;

    @Test
    public void transformDocument() throws Exception {
        final DocumentTransformer documentTransformer = new DocumentTransformer(dataRepository);

        when(document.getDocumentId()).thenReturn("id");
        when(document.getPaperTitle()).thenReturn("title");
        when(document.getPublicationDate()).thenReturn("2017-05-08");

        when(dataRepository.newInstance()).thenReturn(publication);
        when(dataRepository.query().equals(eq("ExternalId"), any()).getOne()).thenReturn(null);

        documentTransformer.transform(document);
        verify(dataRepository).newInstance();
    }

    @Test
    public void transformDocumentException() throws Exception {
        final DocumentTransformer documentTransformer = new DocumentTransformer(dataRepository);

        when(document.getDocumentId()).thenReturn("id");
        when(document.getPaperTitle()).thenReturn("title");
        when(document.getPublicationDate()).thenReturn("thisistest");

        when(dataRepository.newInstance()).thenReturn(publication);
        when(dataRepository.query().equals(eq("ExternalId"), any()).getOne()).thenReturn(null);

        documentTransformer.transform(document);
        verify(dataRepository).newInstance();
    }

    @Test
    public void publicationAlreadyExists() throws Exception {
        final DocumentTransformer documentTransformer = new DocumentTransformer(dataRepository);

        when(document.getDocumentId()).thenReturn("id");
        when(document.getPaperTitle()).thenReturn("title");
        when(document.getPublicationDate()).thenReturn("2017-05-08");

        when(dataRepository.newInstance()).thenReturn(publication);
        when(dataRepository.query().equals(eq("ExternalId"), any()).getOne()).thenReturn(publication);

        Identifiable result = documentTransformer.transform(document);
        assertEquals(publication, result);
    }
}