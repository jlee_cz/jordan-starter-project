package org.czi.kb.fileingestion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import org.czi.kb.fileingestion.objects.Author;
import org.czi.kb.fileingestion.objects.Authorship;
import org.czi.kb.fileingestion.objects.Citation;
import org.czi.kb.fileingestion.objects.Document;
import org.czi.kb.fileingestion.objects.Institution;
import org.czi.kb.fileingestion.parser.AuthorParser;
import org.czi.kb.fileingestion.parser.AuthorshipParser;
import org.czi.kb.fileingestion.parser.CitationParser;
import org.czi.kb.fileingestion.parser.DocumentParser;
import org.czi.kb.fileingestion.parser.InstitutionParser;
import org.junit.Test;

/**
 * These tests read file and parse using appropriate parsers.
 */
public class ParserIntegrationTest {

    @Test
    public void authorFileParse() throws ParseException, IOException {
        List<String> listOfLines = FileUtil.readFile("../service/src/test/resources/author.csv");
        assertTrue(!listOfLines.isEmpty());
        AuthorParser parser = new AuthorParser();

        for (String s: listOfLines) {
            Author i = parser.parse(s);
            assertEquals(s.split(",")[0], i.getAuthorId());
        }
    }

    @Test
    public void authorshipFileParse() throws ParseException, IOException {
        List<String> listOfLines = FileUtil.readFile("../service/src/test/resources/authorship.csv");
        assertTrue(!listOfLines.isEmpty());

        AuthorshipParser parser = new AuthorshipParser();

        for (String s: listOfLines) {
            Authorship i = parser.parse(s);
            assertEquals(s.split(",")[0], i.getAuthorId());
        }
    }


    @Test
    public void citationFileParse() throws ParseException, IOException {
        List<String> listOfLines = FileUtil.readFile("../service/src/test/resources/citation.csv");
        assertTrue(!listOfLines.isEmpty());

        CitationParser parser = new CitationParser();

        for (String s: listOfLines) {
            Citation i = parser.parse(s);
            assertEquals(s.split(",")[0], i.getPaperId());
        }
    }

    @Test
    public void documentFileParse() throws ParseException, IOException {
        List<String> listOfLines = FileUtil.readFile("../service/src/test/resources/document.csv");
        assertTrue(!listOfLines.isEmpty());

        DocumentParser parser = new DocumentParser();

        for (String s: listOfLines) {
            Document i = parser.parse(s);
            assertEquals(s.split(",")[0], i.getDocumentId());
        }
    }

    @Test
    public void institutionFileParse() throws ParseException, IOException {
        List<String> listOfLines = FileUtil.readFile("../service/src/test/resources/institution.csv");
        assertTrue(!listOfLines.isEmpty());

        InstitutionParser parser = new InstitutionParser();

        for (String s: listOfLines) {
            Institution i = parser.parse(s);
            assertEquals(s.split(",")[0], i.getInstitutionId());
        }
    }

}
