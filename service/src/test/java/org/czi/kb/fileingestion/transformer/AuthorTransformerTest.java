package org.czi.kb.fileingestion.transformer;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.czi.kb.fileingestion.objects.Author;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.datagrid.Researcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Using @Mock annotation and instantiating test object inside @Test.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorTransformerTest {

    @Mock
    private static DataRepository<Long, Researcher> dataRepository;

    @Mock
    private static Researcher researcher;

    @Mock
    private static Author author;

    @Test
    public void transformAuthor() throws Exception {

        final AuthorTransformer authorTransformer = new AuthorTransformer(dataRepository);

        // Set up mock methods
        when(author.getAuthorId()).thenReturn("random_id");
        when(author.getAuthorName()).thenReturn("random_name");
        when(dataRepository.newInstance()).thenReturn(researcher);

        authorTransformer.transform(author);

        verify(dataRepository).newInstance();
    }
}
