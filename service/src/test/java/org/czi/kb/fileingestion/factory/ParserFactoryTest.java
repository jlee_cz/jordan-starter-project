package org.czi.kb.fileingestion.factory;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.czi.kb.fileingestion.parser.AuthorParser;
import org.czi.kb.fileingestion.parser.AuthorshipParser;
import org.czi.kb.fileingestion.parser.CitationParser;
import org.czi.kb.fileingestion.parser.DocumentParser;
import org.czi.kb.fileingestion.parser.InstitutionParser;
import org.czi.kb.fileingestion.parser.Parser;
import org.junit.Test;

/**
 * Created by jlee on 09/05/17.
 */
public class ParserFactoryTest {
    private static final AuthorParser authorParser = mock(AuthorParser.class);
    private static final AuthorshipParser authorshipParser = mock(AuthorshipParser.class);
    private static final CitationParser citationParser = mock(CitationParser.class);
    private static final DocumentParser documentParser = mock(DocumentParser.class);
    private static final InstitutionParser institutionParser = mock(InstitutionParser.class);

    private static final ParserFactory parserFactory = new ParserFactory(
            authorParser, authorshipParser, citationParser, documentParser, institutionParser);

    @Test
    public void authorPath() throws ClassNotFoundException {
        Parser p = parserFactory.initParser("some/path/to/file/author.csv");
        assertTrue(p instanceof AuthorParser);
    }

    @Test
    public void authorshipPath() throws ClassNotFoundException {
        Parser p = parserFactory.initParser("some/path/to/file/authorship.csv");
        assertTrue(p instanceof AuthorshipParser);
    }

    @Test
    public void citationPath() throws ClassNotFoundException {
        Parser p = parserFactory.initParser("some/path/to/file/citation.csv");
        assertTrue(p instanceof CitationParser);
    }

    @Test
    public void documentPath() throws ClassNotFoundException {
        Parser p = parserFactory.initParser("some/path/to/file/document.csv");
        assertTrue(p instanceof DocumentParser);
    }

    @Test
    public void institutionPath() throws ClassNotFoundException {
        Parser p = parserFactory.initParser("some/path/to/file/institution.csv");
        assertTrue(p instanceof InstitutionParser);
    }

    @Test (expected = ClassNotFoundException.class)
    public void invalidPath() throws ClassNotFoundException {
        Parser p = parserFactory.initParser("some/path/to/file/INVALID.csv");
    }
}