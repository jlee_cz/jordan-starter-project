package org.czi.kb.fileingestion.factory;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.czi.kb.fileingestion.transformer.AuthorTransformer;
import org.czi.kb.fileingestion.transformer.AuthorshipTransformer;
import org.czi.kb.fileingestion.transformer.CitationTransformer;
import org.czi.kb.fileingestion.transformer.DocumentTransformer;
import org.czi.kb.fileingestion.transformer.InstitutionTransformer;
import org.czi.kb.fileingestion.transformer.Transformer;
import org.junit.Test;

/**
 * Created by jlee on 09/05/17.
 */
public class TransformerFactoryTest {

    private static final AuthorTransformer authorTransformer = mock(AuthorTransformer.class);
    private static final AuthorshipTransformer authorshipTransformer = mock(AuthorshipTransformer.class);
    private static final CitationTransformer citationTransformer = mock(CitationTransformer.class);
    private static final DocumentTransformer documentTransformer = mock(DocumentTransformer.class);
    private static final InstitutionTransformer institutionTransformer = mock(InstitutionTransformer.class);

    private static final TransformerFactory transformerFactory = new TransformerFactory(
            authorTransformer, authorshipTransformer, citationTransformer, documentTransformer, institutionTransformer);

    @Test
    public void authorPath() throws ClassNotFoundException {
        Transformer t = transformerFactory.initTransformer("some/path/to/file/author.csv");
        assertTrue(t instanceof AuthorTransformer);
    }

    @Test
    public void authorshipPath() throws ClassNotFoundException {
        Transformer t = transformerFactory.initTransformer("some/path/to/file/authorship.csv");
        assertTrue(t instanceof AuthorshipTransformer);
    }

    @Test
    public void citationPath() throws ClassNotFoundException {
        Transformer t = transformerFactory.initTransformer("some/path/to/file/citation.csv");
        assertTrue(t instanceof CitationTransformer);
    }

    @Test
    public void documentPath() throws ClassNotFoundException {
        Transformer t = transformerFactory.initTransformer("some/path/to/file/document.csv");
        assertTrue(t instanceof DocumentTransformer);
    }

    @Test
    public void institutionPath() throws ClassNotFoundException {
        Transformer t = transformerFactory.initTransformer("some/path/to/file/institution.csv");
        assertTrue(t instanceof InstitutionTransformer);
    }

    @Test (expected = ClassNotFoundException.class)
    public void invalidPath() throws ClassNotFoundException {
        Transformer t = transformerFactory.initTransformer("src/test/resources/INVALID.csv");
    }
}