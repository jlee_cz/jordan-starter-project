package org.czi.kb.fileingestion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import org.junit.Test;


/**
 * Created by jlee on 08/05/17.
 */
public class FileUtilTest {

    @Test
    public void readAuthorFile() {
        List<String> listOfLines = FileUtil.readFile("src/test/resources/author.csv");

        assertTrue("CHECKING FIRST LINE", listOfLines.get(0).equals(
                "100001179,100001179,\"{}\",\"Sharon\",\"Andrew\",\"{null}\""));

        assertTrue("CHECKING LAST LINE", listOfLines.get(listOfLines.size() - 1).equals(
                "127367624,127367624,\"{}\",\"Matías\",\"Arteaga\",\"{null}\""));
    }

    @Test
    public void readAuthorshipFile() {
        List<String> listOfLines = FileUtil.readFile("src/test/resources/authorship.csv");

        assertEquals(listOfLines.get(0), "100001179,25549718,\"{null}\"");

        assertEquals(listOfLines.get(listOfLines.size() - 1), "127367624,25353639,\"41342\"");
    }

    @Test
    public void returnEmptyList() {
        List<String> listOfLines = FileUtil.readFile("src/test/resources/doesnotexist.csv");
        assertTrue(listOfLines.isEmpty());
    }

    @Test
    public void extractThenDelete() {
        List<String> extractedFilePaths = FileUtil.extractTarFile("src/test/resources/fuse-test-data.tar.gz");
        assertTrue(extractedFilePaths.size() == 5);
        boolean deletedAll = FileUtil.deleteTmpFiles(extractedFilePaths);
        assertTrue(deletedAll);
    }

}
