package org.czi.kb.fileingestion.parser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.Citation;
import org.junit.Test;

/**
 * Created by jlee on 08/05/17.
 */
public class CitationParserTest {
    @Test(expected = ParseException.class)
    public void parseEmpty() throws Exception {
        final CitationParser parser = new CitationParser();
        parser.parse("");
    }



    @Test
    public void parseValid() throws ParseException, IOException {
        final CitationParser citationParser = new CitationParser();
        Citation citation = citationParser.parse("21160398,23412275");

        assertEquals(citation.getPaperId(), "21160398");
        assertEquals(citation.getPaperReferenceId(), "23412275");

    }

    @Test (expected = ParseException.class)
    public void parseInvalid() throws ParseException, IOException {
        final CitationParser citationParser = new CitationParser();
        Citation citation = citationParser.parse("INVALID");

    }
}