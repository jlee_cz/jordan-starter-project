package org.czi.kb.fileingestion.parser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.Institution;
import org.junit.Test;

/**
 * Created by jlee on 08/05/17.
 */
public class InstitutionParserTest {

    @Test(expected = ParseException.class)
    public void parseEmpty() throws Exception {
        final InstitutionParser parser = new InstitutionParser();
        parser.parse("");
    }



    @Test
    public void parseValid() throws ParseException, IOException {
        final InstitutionParser institutionParser = new InstitutionParser();
        Institution institution = institutionParser.parse(
                "20569,20569,\"Kansas State University\",\"{{null}}\",\"Manhattan, KS, US, 66506-0100\"");

        assertEquals(institution.getInstitutionName(), "Kansas State University");
        assertEquals(institution.getInstitutionId(), "20569");
        assertEquals(institution.getInstitutionCity(), "Manhattan, KS, US, 66506-0100");

    }


    @Test (expected = ParseException.class)
    public void parseInvalid() throws ParseException, IOException {
        final InstitutionParser institutionParser = new InstitutionParser();
        Institution institution = institutionParser.parse("SOME INVALID STUFF, \"MORE INVALID\"");

    }
}