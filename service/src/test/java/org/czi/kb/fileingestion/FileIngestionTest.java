package org.czi.kb.fileingestion;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.czi.kb.fileingestion.factory.ParserFactory;
import org.czi.kb.fileingestion.factory.TransformerFactory;
import org.czi.kb.fileingestion.parser.Parser;
import org.czi.kb.fileingestion.transformer.Transformer;
import org.czi.rato.api.cluster.service.MetaServiceContext;
import org.czi.rato.api.cluster.service.Ticket;
import org.czi.rato.api.cluster.service.coordination.Task;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.data.Identifiable;
import org.czi.rato.api.data.Session;
import org.czi.rato.api.data.Transaction;
import org.czi.rato.testing.api.MockMetaServiceContextBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by jlee on 05/05/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class FileIngestionTest {

    @Mock
    private DataRepository<UUID, Task> taskDataRepository;

    @Mock
    private TransformerFactory transformerFactory;

    @Mock
    private ParserFactory parserFactory;

    @InjectMocks
    private FileIngestion fileIngestionService;

    @Test
    public void doService() throws Exception {

        final MockMetaServiceContextBuilder builder = new MockMetaServiceContextBuilder();

        final Ticket ticket = mock(Ticket.class);
        when(ticket.getTaskId()).thenReturn(UUID.randomUUID());
        builder.ticket(ticket);

        final Session session = mock(Session.class);
        final Transaction transaction = mock(Transaction.class);
        when(session.startTransaction()).thenReturn(transaction);
        builder.session(session);

        final MetaServiceContext metaServiceContext = builder.build();


        final Task task = mock(Task.class);
        when(taskDataRepository.get(any())).thenReturn(task);
        final Map<String, String> parameters = new HashMap<>();
        parameters.put(FileIngestion.FILEPATH, "src/test/resources/fuse-test-data.tar.gz");
        when(task.getMetadata()).thenReturn(parameters);


        final Parser textParser = mock(Parser.class);
        final Transformer transformer = mock(Transformer.class);

        when(parserFactory.initParser(any())).thenReturn(textParser);
        when(transformerFactory.initTransformer(any())).thenReturn(transformer);
        when(transformer.transform(any())).thenReturn(mock(Identifiable.class));

        // Actual run
        fileIngestionService.doService(metaServiceContext);

        final int numCalls = 12649 + 15001 + 55490 + 53713 + 427686;
        verify(textParser, times(numCalls)).parse(anyString());
        verify(transformer, times(numCalls)).transform(any());
        verify(session, times(numCalls)).save(any());
        verify(transaction, times(5)).commit();

    }

    @Test
    public void init() throws Exception {
        fileIngestionService.init(null);
    }

    @Test
    public void destroy() throws Exception {
        fileIngestionService.destroy(null);
    }

    @Test
    public void main() throws Exception {
        FileIngestion.main(null);
    }
}

