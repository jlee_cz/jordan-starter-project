package org.czi.kb.fileingestion.transformer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.czi.kb.fileingestion.objects.Authorship;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.datagrid.Institution;
import org.czi.rato.api.model.datagrid.Publication;
import org.czi.rato.api.model.datagrid.Researcher;
import org.czi.rato.api.model.datagrid.relationships.Affiliation;
import org.junit.Test;

/**
 * Using Mockito.mock(), Don't need @RunWith
 */
public class AuthorshipTransformerTest {

    private static final DataRepository dataRepository = mock(DataRepository.class, RETURNS_DEEP_STUBS);
    private static final Affiliation affiliation = mock(Affiliation.class);
    private static final Authorship authorship = mock(Authorship.class);

    // Use same dataRepo otherwise would need to @Mock all of them separately
    private static final AuthorshipTransformer authorshipTransformer
            = new AuthorshipTransformer(dataRepository, dataRepository, dataRepository, dataRepository);


    @Test
    public void transformAuthorship() throws Exception {
        when(authorship.getPaperId()).thenReturn("paper");
        when(authorship.getAuthorId()).thenReturn("author");
        when(authorship.getInstitutionId()).thenReturn("institution");

        when(dataRepository.newInstance()).thenReturn(affiliation);

        // Because chaining together multiple calls, USE RETURNS_DEEP_STUBS
        when(dataRepository.query().equals(any(), eq("author")).getOne()).thenReturn(mock(Researcher.class));
        when(dataRepository.query().equals(any(), eq("institution")).getOne()).thenReturn(mock(Institution.class));
        when(dataRepository.query().equals(any(), eq("paper")).getOne()).thenReturn(mock(Publication.class));

        authorshipTransformer.transform(authorship);

        verify(dataRepository).newInstance();
    }
}