package org.czi.kb.fileingestion.transformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import org.czi.kb.fileingestion.FileIngestion;
import org.czi.kb.fileingestion.objects.Document;
import org.czi.kb.fileingestion.objects.ParsedObject;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.Date;
import org.czi.rato.api.model.datagrid.Publication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Transforms Document Java object to Publication RATO object.
 */
@Component
public class DocumentTransformer implements Transformer {

    private static final Logger logger = LoggerFactory.getLogger(DocumentTransformer.class);
    private final DataRepository<Long, Publication> publicationDataRepository;

    @Autowired
    DocumentTransformer(final DataRepository<Long, Publication> publicationRepository) {
        this.publicationDataRepository = publicationRepository;
    }

    /**
     * take in Paper and transform into Rato Publication.
     * @return rato Publication with id, source, [doi, title, publishDate]
     */
    public Publication transform(final ParsedObject parsedObject) {
        final Document paper = (Document) parsedObject;

        // Check if publication with same external id already exists and return if already exists
        if (publicationDataRepository.query().equals("ExternalId", paper.getDocumentId()).getOne() != null) {
            logger.info("Publication {} already exists", paper.getDocumentId());

            // Return empty instance rather than null
            return publicationDataRepository.newInstance();
        }

        // Rato object creation and fields filling
        final Publication publication = publicationDataRepository.newInstance();
        publication.setSource(FileIngestion.class.getSimpleName());     // "FileIngestion"

        if (paper.getPaperTitle().length() != 0) {
            publication.setArticleTitle(paper.getPaperTitle());
        }

        // This line important. Need to set this otherwise have no way of accessing this paper again
        publication.setExternalId(paper.getDocumentId());


        if (paper.getPublicationDate().length() == 10) {
            try {
                final ZonedDateTime zonedDateTime = new SimpleDateFormat("yyyy-MM-dd")
                        .parse(paper.getPublicationDate()).toInstant().atZone(ZoneId.of("Z"));
                publication.setPubDate(new Date(zonedDateTime.getYear(), zonedDateTime.getMonthValue(),
                        zonedDateTime.getDayOfMonth()));
            } catch (final ParseException exception) {
                logger.error("Failed to parse date: {}", paper.getPublicationDate());
            }
        }
        return publication;
    }
}
