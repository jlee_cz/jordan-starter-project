package org.czi.kb.fileingestion.transformer;

import org.czi.kb.fileingestion.FileIngestion;
import org.czi.kb.fileingestion.objects.Author;
import org.czi.kb.fileingestion.objects.ParsedObject;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.datagrid.Researcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Transforms Author Java object to Researcher RATO object.
 */
@Component
public class AuthorTransformer implements Transformer {

    private final DataRepository<Long, Researcher> researcherDataRepository;

    @Autowired
    public AuthorTransformer(final DataRepository<Long, Researcher> dataRepository) {
        this.researcherDataRepository = dataRepository;
    }

    /**
     * take in Author and transform into Rato Researcher.
     * @param parsedObject intermediate object created for transformation
     * @return rato Researcher with id, name and source
     */
    public Researcher transform(final ParsedObject parsedObject) {
        final Author author = (Author) parsedObject;
        // Rato object creation and fields filling
        final Researcher researcher = researcherDataRepository.newInstance();
        researcher.setNameRaw(author.getAuthorName());
        researcher.setSource(FileIngestion.class.getSimpleName());

        // This line important
        researcher.setExternalId(author.getAuthorId());
        return researcher;
    }


}

