package org.czi.kb.fileingestion.objects;

import org.jetbrains.annotations.NotNull;

/**
 * Institution Java object that gets converted to Institution RATO object.
 */
public class Institution implements ParsedObject {

    private final String institutionId;
    private final String institutionName;
    private final String institutionCity;

    /**
     * Intermediate class corresponding to Institution RATO object.
     * @param info  institution info
     */
    public Institution(@NotNull final String[] info) {
        this.institutionId = info[0];
        this.institutionName = info[2];
        this.institutionCity = info[4];
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public String getInstitutionCity() {
        return institutionCity;
    }
}
