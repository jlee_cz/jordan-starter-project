package org.czi.kb.fileingestion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FileUtil class for file processing.
 * Only contains one method that reads a file, returning the contents in a list.
 */
public class FileUtil {
    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    private FileUtil(){/*Private constructor prevents instantiation*/}

    /**
     * Reads file returning a list of strings, each representing a row of the file.
     * @param localPath     path to file
     * @return              list of strings
     */
    public static List<String> readFile(final String localPath) {

        final List<String> listOfLines = new ArrayList<>();
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(localPath))) {

            while ((line = br.readLine()) != null) {

                listOfLines.add(line);
            }

        } catch (final IOException e) {
            logger.error("Failed to read file", e);
        }

        return listOfLines;


    }

    /**
     * Extracts files from tar.gz and saves them to tmp folder.
     * @param pathToTar     path
     * @return              extracted file paths
     */
    public static List<String> extractTarFile(final String pathToTar) {

        final List<String> extractedFilePaths = new ArrayList<>();

        try (TarArchiveInputStream tarInput = new TarArchiveInputStream(
                new GZIPInputStream(new FileInputStream(pathToTar)))) {

            TarArchiveEntry entry = tarInput.getNextTarEntry();

            while (entry != null) {
                if (entry.isDirectory()) {
                    entry = tarInput.getNextTarEntry();
                    continue;
                }

                // Save to tmp folder
                final File currFile = new File("/tmp/", entry.getName());
                final OutputStream out = new FileOutputStream(currFile);
                IOUtils.copy(tarInput, out);
                out.close();

                logger.info("Extracting to {}", currFile.getAbsolutePath());
                extractedFilePaths.add(currFile.getAbsolutePath());
                entry = tarInput.getNextTarEntry();
            }
            tarInput.close();
        } catch (final IOException e) {
            logger.error("Couldn't read file", e);
        }
        return extractedFilePaths;
    }

    /**
     * Deletes all files in a given list.
     * @param extractedFilePaths     a list of the files to delete
     * @return  true if deleted all files successfully
     */
    public static boolean deleteTmpFiles(final List<String> extractedFilePaths) {

        boolean success = true;
        for (final String path : extractedFilePaths) {
            final File f = new File(path);
            final boolean result = f.delete();
            logger.info("Deleting {} : {}", path, result);

            success = success && result;
        }
        return success;

    }
}
