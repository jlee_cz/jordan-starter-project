package org.czi.kb.fileingestion.objects;

import org.jetbrains.annotations.NotNull;

/**
 * Citation Java object that gets converted to Citation RATO object.
 */
public class Citation implements ParsedObject {

    private final String paperId;
    private final String paperReferenceId;

    /**
     * Intermediate class corresponding to Citation RATO object.
     * @param info  citation info
     */
    public Citation(@NotNull final String[] info) {
        this.paperId = info[0];
        this.paperReferenceId = info[1];
    }

    public String getPaperId() {
        return paperId;
    }

    public String getPaperReferenceId() {
        return paperReferenceId;
    }
}
