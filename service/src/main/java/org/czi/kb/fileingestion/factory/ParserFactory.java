package org.czi.kb.fileingestion.factory;

import org.czi.kb.fileingestion.parser.AuthorParser;
import org.czi.kb.fileingestion.parser.AuthorshipParser;
import org.czi.kb.fileingestion.parser.CitationParser;
import org.czi.kb.fileingestion.parser.DocumentParser;
import org.czi.kb.fileingestion.parser.InstitutionParser;
import org.czi.kb.fileingestion.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ParserFactory manages and controls access to the different parsers.
 * Lifecycle of the parsers is managed by SpringBoot
 */
@Component
public class ParserFactory implements Factory {

    private static final Logger logger = LoggerFactory.getLogger(ParserFactory.class);

    private final AuthorParser authorParser;
    private final AuthorshipParser authorshipParser;
    private final CitationParser citationParser;
    private final DocumentParser documentParser;
    private final InstitutionParser institutionParser;

    @Autowired
    ParserFactory(
                    final AuthorParser authorParser,
                    final AuthorshipParser authorshipParser,
                    final CitationParser citationParser,
                    final DocumentParser documentParser,
                    final InstitutionParser institutionParser) {
        this.authorParser = authorParser;
        this.authorshipParser = authorshipParser;
        this.citationParser = citationParser;
        this.documentParser = documentParser;
        this.institutionParser = institutionParser;
    }

    /**
     * Returns the correct parser given the local path.
     * @param localPath     path to file
     * @return              correct parser
     * @throws ClassNotFoundException   throws if file does not have specified ending
     */
    public Parser initParser(final String localPath) throws ClassNotFoundException {
        // Can't know which parser except from file name; otherwise need external data
        if (localPath.endsWith("author.csv")) {
            return authorParser;
        } else if (localPath.endsWith("authorship.csv")) {
            return authorshipParser;
        } else if (localPath.endsWith("citation.csv")) {
            return citationParser;
        } else if (localPath.endsWith("document.csv")) {
            return documentParser;
        } else if (localPath.endsWith("institution.csv")) {
            return institutionParser;
        } else {
            logger.error("Couldn't parse file {}", localPath);
            throw new ClassNotFoundException("Couldn't parse file " + localPath);
        }
    }
}