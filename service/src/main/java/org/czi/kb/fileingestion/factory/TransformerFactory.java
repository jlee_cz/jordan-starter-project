package org.czi.kb.fileingestion.factory;

import org.czi.kb.fileingestion.transformer.AuthorTransformer;
import org.czi.kb.fileingestion.transformer.AuthorshipTransformer;
import org.czi.kb.fileingestion.transformer.CitationTransformer;
import org.czi.kb.fileingestion.transformer.DocumentTransformer;
import org.czi.kb.fileingestion.transformer.InstitutionTransformer;
import org.czi.kb.fileingestion.transformer.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * TransformerFactory manages and controls access to the different transformers.
 * Lifecycle of the transformers is managed by SpringBoot
 */
@Component
public class TransformerFactory implements Factory {

    private static final Logger logger = LoggerFactory.getLogger(TransformerFactory.class);

    private final AuthorTransformer authorTransformer;
    private final AuthorshipTransformer authorshipTransformer;
    private final CitationTransformer citationTransformer;
    private final DocumentTransformer documentTransformer;
    private final InstitutionTransformer institutionTransformer;

    @Autowired
    TransformerFactory(
            final AuthorTransformer authorTransformer,
            final AuthorshipTransformer authorshipTransformer,
            final CitationTransformer citationTransformer,
            final DocumentTransformer documentTransformer,
            final InstitutionTransformer institutionTransformer) {
        this.authorTransformer = authorTransformer;
        this.authorshipTransformer = authorshipTransformer;
        this.citationTransformer = citationTransformer;
        this.documentTransformer = documentTransformer;
        this.institutionTransformer = institutionTransformer;
    }

    /**
     * Returns the correct transformer given the local path.
     * @param localPath     path to file
     * @return              correct transformer
     * @throws ClassNotFoundException   throws if file does not have specified ending
     */
    public Transformer initTransformer(final String localPath) throws ClassNotFoundException {
        if (localPath.endsWith("author.csv")) {
            return authorTransformer;
        } else if (localPath.endsWith("authorship.csv")) {
            return authorshipTransformer;
        } else if (localPath.endsWith("citation.csv")) {
            return citationTransformer;
        } else if (localPath.endsWith("document.csv")) {
            return documentTransformer;
        } else if (localPath.endsWith("institution.csv")) {
            return institutionTransformer;
        } else {
            logger.error("Couldn't transform file {}", localPath);
            throw new ClassNotFoundException("Couldn't transform file " + localPath);
        }
    }
}
