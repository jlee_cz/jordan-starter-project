package org.czi.kb.fileingestion.transformer;

import org.czi.kb.fileingestion.objects.ParsedObject;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.datagrid.Institution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Transforms Institution Java object to Institution RATO object. Note these are not the same
 * despite having the same names.
 */
@Component
public class InstitutionTransformer implements Transformer {

    private final DataRepository<Long, Institution> institutionRepository;

    @Autowired
    InstitutionTransformer(final DataRepository<Long, Institution> institutionRepository) {
        this.institutionRepository = institutionRepository;
    }

    @Override
    public Institution transform(ParsedObject input) {
        org.czi.kb.fileingestion.objects.Institution institutionJava = (org.czi.kb.fileingestion.objects.Institution) input;

        final Institution institution = institutionRepository.newInstance();

        institution.setName(institutionJava.getInstitutionName());
        institution.setCity(institutionJava.getInstitutionCity());

        return institution;
    }
}
