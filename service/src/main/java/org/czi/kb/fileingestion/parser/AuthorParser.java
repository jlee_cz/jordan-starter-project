package org.czi.kb.fileingestion.parser;

import com.opencsv.CSVParser;
import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.Author;
import org.springframework.stereotype.Component;

/**
 * Parses a line from a CSV file and creates an Author Object from the info.
 */
// Need @COMPONENT because used as part of SpringBoot thus for @Autowired to work, must have this
@Component
public class AuthorParser implements Parser {

    @Override
    public Author parse(final String line) throws ParseException, IOException {

        final CSVParser csvParser = new CSVParser(',','"');
        final String[] tokens = csvParser.parseLine(line);

        if (tokens.length == 6) {
            return new Author(tokens);
        } else {
            throw new ParseException("Intermediate parse syntax error", 0);
        }
    }


}
