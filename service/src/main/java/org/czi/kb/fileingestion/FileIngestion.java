package org.czi.kb.fileingestion;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.czi.kb.fileingestion.factory.ParserFactory;
import org.czi.kb.fileingestion.factory.TransformerFactory;
import org.czi.kb.fileingestion.objects.ParsedObject;
import org.czi.kb.fileingestion.parser.Parser;
import org.czi.kb.fileingestion.transformer.Transformer;
import org.czi.rato.api.cluster.service.InitializationContext;
import org.czi.rato.api.cluster.service.MetaService;
import org.czi.rato.api.cluster.service.MetaServiceContext;
import org.czi.rato.api.cluster.service.MetaServiceInfo;
import org.czi.rato.api.cluster.service.coordination.Task;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.data.Identifiable;
import org.czi.rato.api.data.Session;
import org.czi.rato.api.data.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * File ingestion service that takes MetaServiceContext, gets the appropriate CSV file, gets the correct parser
 * and transformer, and then converts the data into RATO objects which get saved.
 */
@MetaServiceInfo(majorVersion = 0,
        minorVersion = 1,
        description = "File Ingestion Service for Starter Project"
)
@SpringBootApplication
@ComponentScan({"org.czi.kb.fileingestion", "org.czi.kb.testing.config"})
public class FileIngestion implements MetaService {

    private static final Logger logger = LoggerFactory.getLogger(FileIngestion.class);

    static final String FILEPATH = "org.czi.kb.fileingestion.filepath";

    @Autowired
    private DataRepository<UUID, Task> taskRepo;

    @Autowired
    private ParserFactory parserFactory;

    @Autowired
    private TransformerFactory transformerFactory;


    /**
     * Main class place holder for spring boot.
     */
    public static void main(final String[] args) {
        final MetaServiceInfo info = FileIngestion.class.getAnnotation(MetaServiceInfo.class);
        logger.info("{} {} {}", info.description(), info.majorVersion(), info.minorVersion());

    }


    /**
     * Static file ingestion service that takes in a context which contains a filepath.
     * Service then parses csv file, creating intermediate java objects, then converts those into RATO objects.
     * @param context       context that contains filepath
     */
    @Override
    public void doService(final MetaServiceContext context) {
        logger.info("File Ingestion Service Running");

        // parameter for this task
        final UUID uuid = context.getTicket().getTaskId();
        final Map<String, String> parameters = taskRepo.get(uuid).getMetadata();
        logger.info("The metadata is: {}", parameters);

        final String targzPath = parameters.get(FILEPATH);

        final List<String> extractedFilePaths = FileUtil.extractTarFile(targzPath);

        for (final String localPath : extractedFilePaths) {
            // example of starting transaction, notice it is committed at the end.
            final Session session = context.getContext().getFactory().getOrCreateSession();
            final Transaction tx = session.startTransaction();

            final Long time = System.currentTimeMillis();

            saveSingleFile(session, localPath);

            // transactions should last short (< 1 sec) amount of time, or there could be undefined behavior
            tx.commit();
            logger.info("Time spend processing {}: {} ms", localPath, System.currentTimeMillis() - time);

        }

        FileUtil.deleteTmpFiles(extractedFilePaths);
        logger.info("File Ingestion Service finished");
    }

    private void saveSingleFile(final Session session, final String localPath) {

        // Init appropriate parser and transformer
        final Parser parser;
        final Transformer transformer;
        try {
            parser = parserFactory.initParser(localPath);
            transformer = transformerFactory.initTransformer(localPath);
        } catch (ClassNotFoundException e) {
            logger.error("Could not init parser or transformer. Exiting service", e);
            return;
        }

        /*
         * In practice, not using FileUtil and directly reading line by line and processing immediately saves memory
         * as a list of strings isn't being allocated
         * This is important with large datasets to prevent an OutOfMemoryError
         */

        // Convert lines to intermediate Java objects then to RATO objects which get saved
        String line = "";
        Long lineCount = 0L;
        try (BufferedReader br = new BufferedReader(new FileReader(localPath))) {

            while ((line = br.readLine()) != null) {
                final ParsedObject parsedObject = parser.parse(line);
                final Identifiable identifiable = transformer.transform(parsedObject);

                // Save isn't "official" until transaction is committed
                session.save(identifiable);

                lineCount++;
            }

        } catch (final IOException e) {
            logger.error("Failed to read file", e);
        } catch (final ParseException e) {
            logger.error("Could not parse {} from file {}", line, localPath);
            logger.error("ParseException", e);
        }


        logger.info("Complete processing {}; {} lines", localPath, lineCount);
    }

    @Override
    public void init(final InitializationContext initializationContext) {
        logger.info("Initialized File Ingestion service");
    }

    @Override
    public void destroy(final InitializationContext initializationContext) {
        logger.info("Destroyed File Ingestion service");
    }

}

