package org.czi.kb.fileingestion.parser;

import com.opencsv.CSVParser;
import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.Document;
import org.springframework.stereotype.Component;

/**
 * Parses a line from a CSV file and creates a Document Object from the info.
 */
// Need @COMPONENT because used as part of SpringBoot thus for @Autowired to work, must have this
@Component
public class DocumentParser implements Parser {

    @Override
    public Document parse(final String line) throws ParseException, IOException {

        final CSVParser csvParser = new CSVParser(',','"');
        final String[] tokens = csvParser.parseLine(line);

        if (tokens.length == 12) {
            return new Document(tokens);
        } else {
            throw new ParseException("mismatching syntax, expecting 12 tokens, given " + tokens.length, 0);
        }
    }


}
