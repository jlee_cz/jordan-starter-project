package org.czi.kb.fileingestion.objects;

import org.jetbrains.annotations.NotNull;

/**
 * Author Java object that gets converted to Researcher RATO object.
 */
public class Author implements ParsedObject {

    private final String authorId;
    private final String firstName;
    private final String lastName;

    /**
     * Intermediate class to be converted to Researcher RATO object.
     * @param info      author info
     */
    public Author(@NotNull final String[] info) {
        this.authorId = info[0];
        this.firstName = info[3];
        this.lastName = info[4];
    }

    public String getAuthorName() {
        return firstName + " " + lastName;
    }

    public String getAuthorId() {
        return authorId;
    }
}
