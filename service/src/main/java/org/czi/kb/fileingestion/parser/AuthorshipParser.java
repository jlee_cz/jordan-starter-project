package org.czi.kb.fileingestion.parser;

import com.opencsv.CSVParser;
import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.Authorship;
import org.springframework.stereotype.Component;

/**
 * Parses a line from a CSV file and creates an Authorship Object from the info.
 */
// Need @COMPONENT because used as part of SpringBoot thus for @Autowired to work, must have this
@Component
public class AuthorshipParser implements Parser {

    @Override
    public Authorship parse(final String line) throws ParseException, IOException {

        final CSVParser csvParser = new CSVParser(',','"');
        final String[] tokens = csvParser.parseLine(line);

        if (tokens.length == 3) {
            return new Authorship(tokens);
        } else {
            throw new ParseException("mismatching syntax, expecting 3 tokens, given", tokens.length);
        }
    }


}
