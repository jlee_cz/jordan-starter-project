package org.czi.kb.fileingestion.objects;

import org.jetbrains.annotations.NotNull;

/**
 * Document Java object that gets converted to Publication RATO object.
 */
public class Document implements ParsedObject {

    private final String documentId;
    private final String publisher;
    private final String title;
    private final String abstractInfo;
    private final String publicationDate;
    private final String language;

    /**
     * Intermediate class corresponding to Publication RATO object.
     * @param info  document info
     */
    public Document(@NotNull final String[] info) {
        this.documentId = info[0];
        this.publisher = info[4];
        this.title = info[5];
        this.abstractInfo = info[6];
        this.publicationDate = info[7];
        this.language = info[9];
    }


    public String getDocumentId() {
        return documentId;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getPaperTitle() {
        return title;
    }

    public String getAbstractInfo() {
        return abstractInfo;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public String getLanguage() {
        return language;
    }
}
