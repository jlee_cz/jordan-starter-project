package org.czi.kb.fileingestion.transformer;

import org.czi.kb.fileingestion.objects.ParsedObject;
import org.czi.rato.api.data.Identifiable;

/**
 * Transformer interface.
 * Common interface for transformers which convert a Java ParsedObject to a RATO Identifiable.
 */
@FunctionalInterface
public interface Transformer {
    Identifiable transform(final ParsedObject input);
}
