package org.czi.kb.fileingestion.parser;

import java.io.IOException;
import java.text.ParseException;
import org.czi.kb.fileingestion.objects.ParsedObject;

/**
 * Parsed Interface.
 * Common interface for the parsers which convert a String line to appropriate Java object.
 */
@FunctionalInterface
public interface Parser {

    /**
     * each class should implement this to parse specific format.
     * @param line comma separated with information of each field of an object
     * @return intermediate java object before convert to rato object
     */
    ParsedObject parse(final String line) throws ParseException, IOException;
}
