package org.czi.kb.fileingestion.objects;

import org.jetbrains.annotations.NotNull;

/**
 * Authorship Java object that gets converted to Affiliation RATO object.
 */
public class Authorship implements ParsedObject {

    private final String authorId;
    private final String paperId;
    private final String institutionId;                  // sometimes {null} othertimes a number

    /**
     * Intermediate class to be converted to Affiliation RATO object.
     * @param info      authorship info
     */
    public Authorship(@NotNull final String[] info) {
        this.authorId = info[0];
        this.paperId = info[1];
        this.institutionId = info[2];
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getPaperId() {
        return paperId;
    }

    public String getInstitutionId() {
        return institutionId;
    }
}
