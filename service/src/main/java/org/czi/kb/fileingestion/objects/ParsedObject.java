package org.czi.kb.fileingestion.objects;

/**
 * Parsed Object Interface.
 * Common interface for the intermediate Java objects created after parsing csv files.
 */
public interface ParsedObject {
}
