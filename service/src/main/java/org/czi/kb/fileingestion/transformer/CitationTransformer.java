package org.czi.kb.fileingestion.transformer;

import org.czi.kb.fileingestion.FileIngestion;
import org.czi.kb.fileingestion.objects.ParsedObject;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.datagrid.Citation;
import org.czi.rato.api.model.datagrid.Publication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Transforms Citation Java object to Citation RATO object. Note these are not the same despite having the same names.
 */
@Component
public class CitationTransformer implements Transformer {

    private final DataRepository<Long, Publication> publicationDataRepository;
    private final DataRepository<Long, Citation> citationDataRepository;

    @Autowired
    CitationTransformer(final DataRepository<Long, Publication> publicationRepository,
                              final DataRepository<Long, Citation> citationRepository) {
        this.publicationDataRepository = publicationRepository;
        this.citationDataRepository = citationRepository;
    }

    /**
     * take in PaperReference and transform into Rato Citation.
     * @param parsedObject intermediate object created for transformation
     * @return rato Citation with fromPaper and toPaper
     */
    public Citation transform(final ParsedObject parsedObject) {
        // Rip naming convention
        final org.czi.kb.fileingestion.objects.Citation paperReference =
                (org.czi.kb.fileingestion.objects.Citation) parsedObject;

        // Rato object creation and linking
        final Citation citation = citationDataRepository.newInstance();

        citation.setSource(FileIngestion.class.getSimpleName());

        final Publication fromPaper = publicationDataRepository.query()
                .equals("ExternalId", paperReference.getPaperId()).getOne();

        final Publication toPaper = publicationDataRepository.query()
                .equals("ExternalId", paperReference.getPaperReferenceId()).getOne();

        citation.setFromPaper(fromPaper);
        citation.setToPaper(toPaper);

        return citation;
    }
}
