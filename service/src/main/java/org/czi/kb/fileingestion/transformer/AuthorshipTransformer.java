package org.czi.kb.fileingestion.transformer;

import org.czi.kb.fileingestion.objects.Authorship;
import org.czi.kb.fileingestion.objects.ParsedObject;
import org.czi.rato.api.data.DataRepository;
import org.czi.rato.api.model.datagrid.Institution;
import org.czi.rato.api.model.datagrid.Publication;
import org.czi.rato.api.model.datagrid.Researcher;
import org.czi.rato.api.model.datagrid.relationships.Affiliation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Transforms Authorship Java object to Affiliation RATO object.
 */
@Component
public class AuthorshipTransformer implements Transformer {

    private final DataRepository<Long, Publication> publicationDataRepository;
    private final DataRepository<Long, Researcher> researcherDataRepository;
    private final DataRepository<Long, Institution> institutionDataRepository;
    private final DataRepository<Long, Affiliation> affiliationDataRepository;

    @Autowired
    AuthorshipTransformer(final DataRepository<Long, Publication> publicationRepository,
                                      final DataRepository<Long, Researcher> researcherRepository,
                                      final DataRepository<Long, Institution> institutionRepository,
                                      final DataRepository<Long, Affiliation> affiliationRepository) {
        this.publicationDataRepository = publicationRepository;
        this.researcherDataRepository = researcherRepository;
        this.institutionDataRepository = institutionRepository;
        this.affiliationDataRepository = affiliationRepository;
    }

    /**
     * take in Authorship and transform into Rato Affiliation.
     * @param parsedObject intermediate object created for transformation
     * @return rato Affiliation with Publication, [Researcher, Institution]
     */
    public Affiliation transform(final ParsedObject parsedObject) {
        final Authorship paperAuthorAffiliation = (Authorship) parsedObject;

        // Rato object creation and linking
        final Affiliation affiliation = affiliationDataRepository.newInstance();

        final String externalId = "ExternalId";

        // Get publication from data repo & copy it to affiliation
        final Publication publication = publicationDataRepository.query()
                .equals(externalId, paperAuthorAffiliation.getPaperId()).getOne();
        affiliation.setPublication(publication);


        final Researcher researcher = researcherDataRepository.query()
                .equals(externalId, paperAuthorAffiliation.getAuthorId()).getOne();
        affiliation.setResearcher(researcher);


        final Institution institution = institutionDataRepository.query()
                .equals(externalId, paperAuthorAffiliation.getInstitutionId()).getOne();
        affiliation.setInstitution(institution);

        return affiliation;
    }

}
